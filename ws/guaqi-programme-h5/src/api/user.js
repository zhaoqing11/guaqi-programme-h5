import request from '@/utils/request'

// export const test = () => {
//   return request.get('/test')
// }

/**
 * 用户下单--获取邀请key
 * @param {*} data
 */
export const getGroupKey = data => {
  return request.post('/api/services/app/NodeSys/GetGroupKey', data)
}

/**
 * 用户下单
 * @param {*} data
 */
export const payOrder = data => {
  return request.post('/api/services/app/NodeSys/PayOrder', data)
}

/**
 * 查看浏览量
 * @param {*} data
 */
export const getGuaQiLog = data => {
  return request.post('/api/services/app/NodeSys/GetGuaQiLog', data)
}

/**
 * 根据邀请key查看用户信息
 * @param {*} data
 */
export const getGroupKeyNumber = data => {
  return request.post('/api/services/app/NodeSys/GetGroupKeyNumber', data)
}

/**
 * 判断用户是否参团/获取参团成员信息
 * @param {*} data
 */
export const getGroupList = data => {
  return request.post('/api/services/app/NodeSys/GetGroupList', data)
}
