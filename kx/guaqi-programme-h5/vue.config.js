const path = require('path')
const defaultSettings = require('./src/settings.js')
// const CompressionPlugin = require('compression-webpack-plugin');

function resolve(dir) {
  return path.join(__dirname, dir)
}

const name = defaultSettings.title || '呱奇编程感恩4周年限时团购活动'

const port = process.env.port || process.env.npm_config_port || 8082 // dev port

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/kx-campus/' : './',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: false,
  productionSourceMap: false,
  devServer: {
    port: port,
    open: false,
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        // target: 'https://mock.apifox.cn/m1/2681883-0-default',
        target: 'http://172.22.26.212:8090/api',
        // target: 'http://47.99.178.229:82/api',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    },
    // 内网穿透 生产环境 开启可IP和域名访问权限
    historyApiFallback: true,
    allowedHosts: 'all'
    // disableHostCheck: true // 绕过主机检查
  },
  // chainWebpack: config => {
  //   config.devServer.disableHostCheck(true)
  // }
  //其他配置
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
    // plugins: [
    //   new CompressionPlugin({
    //       algorithm: 'gzip', // 使用gzip压缩
    //       test: /\.js$|\.html$|\.css$/, // 匹配文件名
    //       filename: '[path].gz[query]', // 压缩后的文件名(保持原文件名，后缀加.gz)
    //       minRatio: 1, // 压缩率小于1才会压缩
    //       threshold: 10240, // 对超过10k的数据压缩
    //       deleteOriginalAssets: false, // 是否删除未压缩的源文件，谨慎设置，如果希望提供非gzip的资源，可不设置或者设置为false（比如删除打包后的gz后还可以加载到原始资源文件）
    //   })
    // ]
  }
}
