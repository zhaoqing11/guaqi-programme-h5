import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/index.vue'
import M403 from '../views/403.vue'
import M404 from '../views/404.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: '首页',
      needLogin: false
    }
  },
  {
    path: '/courseBenefits',
    name: 'CourseBenefits',
    component: () => import('../views/courseBenefits.vue')
  },
  {
    path: '/giftBenefits',
    name: 'GiftBenefits',
    component: () => import('../views/giftBenefits.vue')
  },
  {
    path: '/activityRule',
    name: 'ActivityRule',
    component: () => import('../views/activityRule.vue')
  },
  {
    path: '/companyProfile',
    name: 'CompanyProfile',
    component: () => import('../views/companyProfile.vue')
  },
  {
    path: '/teachingServices',
    name: 'TeachingServices',
    component: () => import('../views/teachingServices.vue')
  },
  {
    path: '/product',
    name: 'Product',
    component: () => import('../views/product.vue')
  },
  {
    path: '/success',
    name: 'Success',
    component: () => import('../views/success.vue')
  },
  {
    path: '/403',
    name: 'M403',
    component: M403,
    meta: {
      title: '403',
      needLogin: false
    }
  },
  {
    path: '/:pathMatch(.*)*',
    component: M404
  }
]

const router = createRouter({
  history: createWebHistory(
    process.env.NODE_ENV === 'production' ? '/kx-campus/' : '/'
  ),
  routes
})

// 白名单
const WHITE_LIST = [
  '/',
  '/courseBenefits',
  '/giftBenefits',
  '/activityRule',
  '/companyProfile',
  '/teachingServices',
  '/MP_verify_pd4hzfEZLl9aSVCj.txt'
]

// 路由守卫-前置处理器/拦截器
router.beforeEach((to, from, next) => {
  // console.log(to.name, from.name)
  if (to.path === '/login') {
    if (window.localStorage.getItem('token')) {
      next({
        path: '/'
      }) // 若token存在，进入首页
    } else {
      // 直接放行 进入登录页
      next()
    }
  } else {
    // if (WHITE_LIST.includes(to.path)) {
    //   next()
    // } else {
    if (window.localStorage.getItem('token')) {
      // 直接放行 进入该页面
      if (to.path === '/about') {
        next({
          path: '/403'
        })
      } else {
        next()
      }
    } else {
      next()

      // 进入登录页
      // next({
      //   path: '/login'
      // })
    }
    // }
  }
  next()
})

export default router
