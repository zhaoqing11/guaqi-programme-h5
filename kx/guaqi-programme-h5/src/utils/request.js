import axios from 'axios'

// 修改全局的baseURL
axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL

// 全局请求拦截处理
axios.interceptors.request.use(config => {
  // console.log('req config->', config)
  // 请求拦截 给每个请求添加token
  config.headers.token = window.localStorage.getItem('token')
  return config
})

// 全局响应拦截处理
axios.interceptors.response.use(res => {
  // console.log('res->', res)

  if (res.data.code !== 0) {
    // return error
  }
  return res
})

export default axios
